Čo sa týka riešenia zvolil som si na dané zadanie použiť Blazor webovú aplikáciu.
Na perzistenciu dát využívam službu so singleton životnosťou, aby sa dali otestovať požadované kroky v zadaní.
Čo sa týka designu aplikácie, urobil som len minimálne vhľadové zmeny. Výstup ktorý aplikácia vypisuje je oddelený farbami :

Zelená - novo pripadné súbory
Modrá - modifikované súbory (+ zmena verzie)
Červená - zmazané súbory

Spomenul by som jedno dôležité obmedzenie. Podľa zadania a príkladu, má aplikácia vypisovať len zmeny na súboroch v danom priečinku a
v podpriečinkoch, nevypisuje ale vo výstupe samotné priečinky. Preto ak pridáme či odoberieme prázdny priečinok, analýza nám túto zmenu neukáže.
Ak sa však v podpriečinkoch dejú nejaké zmeny na súborov (pridanie, modifikovanie, odstránenie), tak sú tieto súbory vypísané
aj s podcestou kde sa nachádzajú.

Ďalšie omedzenie je ešte to, že aplikácia nebude fungovať na ceste kde nemá povolenie k čítaniu súborov a tento prípad som neriešil.

Dáta a modelovú časť mám uložené v stromovej štruktúre, kde často používam rekurziu na prehliadavanie.

Aplikáciu som testoval na OS windows a v rámci testovacieho zadania som neriešil použiteľnosť na ostatných platformách.


