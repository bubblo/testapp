﻿using TestApp.Models;

namespace TestApp.Services.FolderAnalyzer;

public interface IFolderAnalyzer
{
    bool IsFolderExist(string directoryPath);
    FolderInfoModel GetFolderFileInfoList(string directoryPath);
}