﻿namespace TestApp.Helper;

public enum FileState
{
    Unmodified,
    Modified,
    New,
    Removed
}