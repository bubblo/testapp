﻿using Microsoft.AspNetCore.Components;
using TestApp.Helper;
using TestApp.Models;

namespace TestApp.Components.Container;

public partial class FileContainer : ComponentBase
{
    [Parameter] public required FileInfoModel FileInfoModel { get; set; }
    [Parameter] public required string FullName { get; set; }
    private string FileStateClass => GetFileStateClass();

    private string GetFileStateClass()
    {
        switch (FileInfoModel.FileState)
        {
            case FileState.Unmodified:
                return "file__unmodified";
            case FileState.Modified:
                return "file__modified";
            case FileState.New:
                return "file__new";
            case FileState.Removed:
                return "file__removed";
            default:
                throw new ArgumentException("FileState wrong type.");
        }
    }
}