﻿using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;
using TestApp.Models;
using TestApp.Services.FolderAnalyzer;

namespace TestApp.Components.Pages;

public partial class LandingPage : ComponentBase
{
    [Inject] public IFolderAnalyzer FolderAnalyzer  { get; set; } = default!;
    [Inject] public IJSRuntime JsRuntime { get; set; } = default!;
    [SupplyParameterFromForm] public string FolderPath { get; set; } = string.Empty;

    private FolderInfoModel? _folderInfoModel;
    private bool _pathHasCorrectFormat = true;

    private void OnClickAnalyzeDirectory()
    {
        _pathHasCorrectFormat = FolderAnalyzer.IsFolderExist(FolderPath);
        if (_pathHasCorrectFormat)
        {
            _folderInfoModel = FolderAnalyzer.GetFolderFileInfoList(FolderPath);
        }
    }
}