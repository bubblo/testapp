﻿using TestApp.Helper;

namespace TestApp.Models;

public class FileInfoModel
{
    public string Name { get; }
    public FileState FileState { get;  set; }
    public uint Version { get; private set;}
    private DateTime LastWriteTime { get; set; }

    public FileInfoModel(string name, DateTime lastWriteTime)
    {
        Name = name;
        LastWriteTime = lastWriteTime;
        Version = 1;
        FileState = FileState.New;
    }

    public void TrySetLastModified(DateTime lastWriteTime)
    {
        if (lastWriteTime > LastWriteTime)
        {
            LastWriteTime = lastWriteTime;
            Version++;
            FileState = FileState.Modified;
        }
        else
        {
            FileState = FileState.Unmodified;
        }
    }

}